# Tofu Menu
(formerly Fedora Menu)


Menu similar to Apple macOS menu for the GNOME Desktop

fork of [Big Sur Menu by fausto](https://extensions.gnome.org/extension/3703/big-sur-menu/)
Compatible with and tested on GNOME 40. Should work on older versions.

Join the Discord for the latest news and releases: https://discord.gg/hfARC5dF

### Dependencies:

* `xkill` required for Force Quit App
*  `gnome-extensions-app` required for Extensions.


# Installation


use GNU make:

    make install


***

## Credits

[@kaansenol5](https://github.com/kaansenol5) , [@ShrirajHegde](https://github.com/ShrirajHegde), [@AndrewZaech](https://github.com/AndrewZaech), [@vikashraghavan](https://github.com/vikashraghavan) - help with development

[@Fausto-Korpsvart](https://github.com/Fausto-Korpsvart), [Frippery Applications Menu](https://extensions.gnome.org/extension/13/applications-menu/) - Original Code

***

This project is not officially connected with Fedora, Red Hat, or any associated entity.
